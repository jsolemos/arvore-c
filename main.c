#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char FILE_NAME[] = "./dados.txt";
char FILE_NAME_TMP[] = "./dados~";

typedef struct info
{
    long cpf;
    int id, linha;
} Info;

typedef struct no
{
    struct info *dados;
    struct no *esq, *dir;
} NoArv;

Info *criarInfo()
{
    return (Info *)malloc(sizeof(Info));
}

NoArv *criarNoArv()
{
    NoArv *a = (NoArv *)malloc(sizeof(NoArv));
    Info *dados = criarInfo();
    a->dados = dados;
    return a;
}

int isNull(NoArv *a)
{
    return a == NULL;
}

/*Returns the length of the str passed*/
int length(char *str)
{
    int characterCount = 0;

    while (str[characterCount] != '\0')
    {
        characterCount++;
    }
    return characterCount;
}

/*Starts looking for substring parameter in string from the passed start index*/
int indexOf(char *string, char *subString, int start)
{
    int flag;
    int indexOf = -1;
    int loopEnd = (int)(length(string) - length(subString));

    for (int i = start; i <= loopEnd; i++)
    {
        flag = 1;
        for (int index = 0; index < length(subString); index++)
        {
            if (string[i] != subString[index])
            {
                flag = 0;
                break;
            }
            else
            {
                i++;
            }
        }
        if (flag == 1)
        {
            indexOf = (int)(i - length(subString));
            break;
        }
    }

    return indexOf;
}

/*Helper function that returns the count of a particular delimiter in the string passed*/
int countOf(char *string, char *searchString)
{
    int index;
    int delimiterTally = 0;
    int start = 0;

    while (1)
    {
        index = indexOf(string, searchString, start);
        if (index != -1)
        {
            delimiterTally++;
        }
        else
        {
            break;
        }
        start = index + 1;
    }

    return delimiterTally;
}

/*Helper function that returns the substring*/
char *subString(char *string, int start, int length)
{
    char *a = (char *)malloc(length + 1);
    int index;

    for (index = 0; index < length; index++)
    {
        a[index] = string[index + start];
    }

    a[index] = '\0';

    return a;
}

/*Main driver function*/
char *string_replace(char *string, char *replaceFor, char *replaceWith)
{
    if (string == NULL || replaceFor == NULL || replaceWith == NULL)
    {
        return NULL;
    }

    int index;
    int subStringCount = countOf(string, replaceFor);

    /*One option to free the allocated memory here is for the caller to handle it. 
     
     Another approach is to have the buffer as a method input parameter to make our intentions of who's
     
     going to handle the freeing part very clear
     */
    char *replacedString = (char *)malloc(length(string) - length(replaceFor) * subStringCount + length(replaceWith) * subStringCount);
    int start = 0, prevCount = 0, count = 0;

    while (1)
    {
        index = indexOf(string, replaceFor, start);
        if (index != -1)
        {
            for (int i = 0; i < index - prevCount; i++)
            {
                replacedString[count] = string[i + prevCount];
                count++;
            }
            for (int i = 0; i < length(replaceWith); i++)
            {
                replacedString[count] = replaceWith[i];
                count++;
            }
            prevCount = (int)(index + length(replaceFor));
        }
        else
        {
            break;
        }
        start = (int)(index + length(replaceFor));
    }

    char *temp = subString(string, prevCount, (int)length(string) - prevCount);

    for (int i = 0; i < length(temp); i++)
    {
        replacedString[count] = temp[i];
        count++;
    }

    replacedString[count] = '\0'; /*Terminating character*/

    return replacedString;
}

NoArv *inserir(NoArv *a, Info *dados)
{
    if (dados == NULL)
        return NULL;

    if (a == NULL)
    {
        a = criarNoArv();
        a->dados = dados;
        a->esq = a->dir = NULL;
    }
    else if (dados->cpf < a->dados->cpf)
    {
        a->esq = inserir(a->esq, dados);
    }
    else if (dados->cpf > a->dados->cpf)
    {
        a->dir = inserir(a->dir, dados);
    }
    return a;
}

void imprimirInfo(NoArv *a)
{
    if (a != NULL)
    {
        printf("Linha: %d, CPF:%ld  ID: %d\n", a->dados->linha, a->dados->cpf, a->dados->id);
    }
}

NoArv *liberar(NoArv *a)
{
    if (!isNull(a))
    {
        liberar(a->esq); /* libera sae */
        liberar(a->dir); /* libera sad */
        free(a);         /* libera raiz */
    }
    return NULL;
}

NoArv *consultar(NoArv *a, long cpf)
{
    if (a == NULL)
        return NULL;

    if (a->dados->cpf == cpf)
    {
        return a;
    }
    else if (a->dados->cpf < cpf)
    {
        return consultar(a->dir, cpf);
    }
    else if (a->dados->cpf > cpf)
    {
        return consultar(a->esq, cpf);
    }
    return NULL;
}

void copy_tmp_file(char origem[], char destino[])
{
    char cmd[256];
    sprintf(cmd, "/bin/cp -p \'%s\' \'%s\'; /bin/rm %s", origem, destino, origem);
    system(cmd);
}

char **str_split(char str[256])
{
    char *temp = 0;
    char **result = 0;
    unsigned int tamanho = 0;

    temp = strtok(str, ",");

    if (temp)
    {
        result = malloc((tamanho + 1) * sizeof(char **));
        result[tamanho++] = temp;
    }

    while ((temp = strtok(0, ",")) != 0)
    {
        result = realloc(result, (tamanho + 1) * sizeof(char **));
        result[tamanho++] = temp;
    }

    // for (int i = 0; i < tamanho; i++)
    // printf("%d - %s\n", i, result[i]);

    return result;
}

Info *preencheInfo(char str[256], int nLinha)
{
    Info *info = criarInfo();

    if (info == NULL)
    {
        printf("Memoria indisponivel");
        return NULL;
    }
    char **result = str_split(str);
    info->id = atoi(result[0]);
    info->linha = (nLinha + 1);
    info->cpf = atol(result[3]);
    return info;
}

NoArv *preencherArvore(NoArv *a)
{
    FILE *arq;
    int result, i = 0;
    char linha[256];

    arq = fopen(FILE_NAME, "rt");
    if (arq == NULL)
    {
        printf("Erro ao criarNoArv arquivo de dados.");
        return NULL;
    }

    while (!feof(arq))
    {
        result = (char)fgets(linha, 256, arq);
        if (result)
        {
            // printf("Linha %d : %s", i, linha);
            Info *dados = preencheInfo(linha, i);
            a = inserir(a, dados);
            // printf("-> %i\n", i);
            // imprimirInfo(a);
        }
        i++;
    }
    fclose(arq);
    return a;
}

void removeLineArquivo(int nLine)
{
    printf("remover linha %i...\n", nLine);
    FILE *arq = fopen(FILE_NAME, "rt");
    FILE *tmpfile = fopen(FILE_NAME_TMP, "w");
    char linha[256];
    int result, i = 1, aux = 0;
    while (!feof(arq))
    {
        result = (char)fgets(linha, 256, arq);
        if (result)
        {
            if (i != nLine)
            {
                fputs(linha, tmpfile);
                aux++;
            }
        }
        i++;
    }

    fclose(tmpfile);
    fclose(arq);
    if (aux > 0)
        copy_tmp_file(FILE_NAME_TMP, FILE_NAME);
    // sleep(1);
}

void freeTree(NoArv *r)
{
    imprimirInfo(r);
    // if(r->dir == NULL && r->esq == NULL)
    removeLineArquivo(r->dados->linha);
    free(r);
}

NoArv *remover(NoArv *r, long v)
{

    if (r == NULL)
        return NULL;
    else if (r->dados->cpf > v)
        r->esq = remover(r->esq, v);
    else if (r->dados->cpf < v)
        r->dir = remover(r->dir, v);
    else
    { /* achou o nó a remover */
        /* nó sem filhos */
        if (r->esq == NULL && r->dir == NULL)
        {
            freeTree(r);
            r = NULL;
        }
        /* nó só tem filho à direita */
        else if (r->esq == NULL)
        {
            NoArv *t = r;
            r = r->dir;
            r->dados->linha = r->dados->linha - 1;
            freeTree(t);
        }
        /* só tem filho à esquerda */
        else if (r->dir == NULL)
        {
            NoArv *t = r;
            r = r->esq;
            r->dados->linha = r->dados->linha - 1;
            freeTree(t);
        }
        /* nó tem os dois filhos */
        else
        {
            int nLineRm = r->dados->linha;
            NoArv *f = r->esq;
            while (f->dir != NULL)
            {
                f = f->dir;
            }
            // printf("Move...\n");
            // imprimirInfo(f);
            // printf("Para(rm)...\n");
            // imprimirInfo(r);
            // printf("-----------\n");
            // removeLineArquivo(nLineRm);

            NoArv *cp = criarNoArv();
            cp->dados->cpf = r->dados->cpf;
            cp->dados->id = r->dados->id;
            cp->dados->linha = r->dados->linha;

            r->dados->cpf = f->dados->cpf; /* troca as informações */
            r->dados->id = f->dados->id;   /* troca as informações */

            // f->dados->linha = f->dados->linha-1;

            r->dados->linha = f->dados->linha; /* troca as informações */

            f->dados->cpf = cp->dados->cpf;
            f->dados->id = cp->dados->id;
            f->dados->linha = cp->dados->linha;

            free(cp);

            // printf("+++++++++++\n");
            // imprimirInfo(r->esq);
            // printf("+++++++++++\n");
            r->esq = remover(r->esq, v);
        }
    }
    r = liberar(r);
    r = preencherArvore(r);
    return r;
}

void imprimirPreordem(NoArv *a)
{
    if (!isNull(a))
    {
        imprimirInfo(a);          /* mostra raiz */
        imprimirPreordem(a->esq); /* mostra sae */
        imprimirPreordem(a->dir); /* mostra sad */
    }
}

void imprimirEmordem(NoArv *a)
{
    if (a != NULL)
    {
        imprimirEmordem(a->esq);
        imprimirInfo(a);
        imprimirEmordem(a->dir);
    }
}

void imprimirPosordem(NoArv *a)
{
    if (!isNull(a))
    {
        imprimirPosordem(a->esq); /* mostra sae */
        imprimirPosordem(a->dir); /* mostra sad */
        imprimirInfo(a);          /* mostra raiz */
    }
}

void alterarNomeArquivo(NoArv *r, char nome[])
{
    if (isNull(r))
        return;
    FILE *arq = fopen(FILE_NAME, "rt");
    FILE *tmpfile = fopen(FILE_NAME_TMP, "w");
    char linha[256];
    int result, i = 1, aux = 0;
    while (!feof(arq))
    {
        result = (char)fgets(linha, 256, arq);
        if (result)
        {
            if (i == r->dados->linha)
            {
                char cpLinha[256];
                strcpy(cpLinha, linha);
                char **row = str_split(linha);
                // printf("EM:%s\n", linha);
                // printf("Substituir %s -> %s\n", row[1], nome);

                char nnL[256] = "";
                int u;
                char auxS[100];
                for (u = 0; u <= 4; u++)
                {
                    // printf("%i-%s\n", u, row[u]);
                    if (u != 0)
                        strcat(nnL, ",");
                    strcpy(auxS, u == 1 ? nome : row[u]);
                    strcat(nnL, auxS);
                }

                fputs(nnL, tmpfile);
                aux++;
            }
            else
            {
                fputs(linha, tmpfile);
            }
        }
        i++;
    }

    fclose(tmpfile);
    fclose(arq);
    if (aux > 0)
        copy_tmp_file(FILE_NAME_TMP, FILE_NAME);
    // sleep(1);
}

void alterarNome(NoArv *a, long cpf, char nome[])
{
    NoArv *aux = consultar(a, cpf);
    alterarNomeArquivo(aux, nome);
}

/*
void callOpt(NoArv *r, int opt)
{
    long cpf;
    char aux[100];

    switch (opt)
    {
    case 1:

        printf("Informe o cpf:\n");
        // scanf("%lu", &cpf);
        NoArv * a = consultar(r, cpf);
        imprimirInfo(a);
        printf("---------------------\n");
        break;
    case 2:

        printf("Informe o cpf:\n");
        gets(aux);   
        cpf = atol(aux);     
        printf("Informe o nome para alterar:\n\n");
        gets(aux);
        alterarNome(r, cpf, aux);
        break;
    case 3:
        printf("Informe o cpf:\n");
        // scanf("%lu", &cpf);
        remover(r, cpf);
        break;
    case 4:
        imprimirPreordem(r);
        break;
    default:
        printf("Opção não encontrada.\n");
        break;
    }
}


int menu()
{
    printf("MENU\n");
    printf("==============================\n");
    printf("1 - Buscar por CPF\n");
    printf("2 - Editar nome por CPF\n");
    printf("3 - Remover por CPF\n");
    printf("4 - Imprimir Arvore Completa\n");
    printf("5 - Sair\n");
    printf("==============================\n");
    printf("Selecione uma opção\n");
    int opt = 0;
    scanf("%i", &opt);
    return opt;
} */

int main()
{
    NoArv *a = NULL;
    a = preencherArvore(a);
    /* int opt = 0;
    do{
        opt = menu();
        // callOpt(r, opt);
        
    }while(opt != 5); */
    
    printf("Arvore Completa\n");
    imprimirPreordem(a);
    printf("====================\n");

    long CPF1 = 3867194547;
    printf("Consultando CPF: %ld\n------\n", CPF1);
    NoArv *b = consultar(a, CPF1);
    imprimirInfo(b);

    long CPF2 = 3867194548;
    a = remover(a, CPF2);

    sleep(2);

    long CPF3 = 38671945444;
    a = remover(a, CPF3);

    sleep(2);

    printf("Editar nome CPF: %ld \n", 3867194549);
    alterarNome(a, 3867194549, "Manuela");
    sleep(2);
    printf("Editar nome CPF: %ld \n", 3867194542);
    alterarNome(a, 3867194542, "João Junior");
    sleep(2);
    printf("Editar nome CPF: %ld \n", 3867194547);
    alterarNome(a, 3867194547, "Carolina");

    printf("====================\n");
    printf("Arvore Completa\n");
    imprimirPreordem(a);
    printf("====================\n");

    liberar(a);

    return 0;
}
